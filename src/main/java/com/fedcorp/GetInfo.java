package com.fedcorp;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class GetInfo {

    private Object o;

    public GetInfo(Object o) {
        this.o = o;
    }
    public void classInfo(){
        Class clazz = o.getClass();
        Annotation [] annotations;

        System.out.println("Class name - " + clazz.getName());

        annotations = clazz.getDeclaredAnnotations();
        if(annotations.length != 0){
            System.out.println("Class Annotations:");
            for (Annotation a : annotations) System.out.println(a);
        }else System.out.println("Class has no annotations");

        System.out.println("--------------------------------------------------------");
        Constructor[] constructors = clazz.getDeclaredConstructors();
        if(constructors.length != 0){
            System.out.println("Class Constructors:");
            for (Constructor a : constructors){
                annotations = a.getDeclaredAnnotations();
                if(annotations.length != 0) {
                    for (Annotation annotation : annotations) {
                        System.out.println(annotation.toString() + " ");
                    }
                }else System.out.println("Constructor has no annotations");
                System.out.print(Modifier.toString(a.getModifiers())+ " ");
                System.out.print(a.getName() + "(");
                for (Class tmp : a.getParameterTypes()){
                    System.out.print(tmp.getName() + "  ");
                }
                System.out.println(")");
                System.out.println();
            }
        }

        System.out.println("--------------------------------------------------------");
        Field[] fields = clazz.getDeclaredFields();
        if(fields.length != 0){
            System.out.println("Class Fields:");
            for (Field a : fields){
                annotations = a.getDeclaredAnnotations();
                if(annotations.length != 0) {
                    for (Annotation annotation : annotations) {
                        System.out.println(annotation.toString() + " ");
                    }
                }else System.out.println("Field has no annotations");
                System.out.print(Modifier.toString(a.getModifiers())+ " ");
                System.out.print(a.getType()+ " ");
                System.out.println(a.getName());
                System.out.println();
            }
        }

        System.out.println("--------------------------------------------------------");
        Method[] methods = clazz.getDeclaredMethods();
        if(methods.length != 0){
            System.out.println("Class Methods:");
            for (Method a : methods){
                annotations = a.getDeclaredAnnotations();
                if(annotations.length != 0) {
                    for (Annotation annotation : annotations) {
                        System.out.println(annotation.toString() + " ");
                    }
                }else System.out.println("Method has no annotations");
                System.out.print(Modifier.toString(a.getModifiers())+ " ");
                System.out.print(a.getReturnType() + " ");
                System.out.print(a.getName() + "(");
                for (Class tmp : a.getParameterTypes()){
                    System.out.print(tmp.getName() + "  ");
                }
                System.out.println(")");
                System.out.println();
            }
        }
    }

}
