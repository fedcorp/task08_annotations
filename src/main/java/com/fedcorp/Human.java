package com.fedcorp;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class Human {
    @Imperio(name = "Олег")
    public String firstName;
    public String lastName;
    @Imperio(age = 29)
    private int age;


    Human(String firstName, String lastName, int age){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    //Перевіряє поля класу, якщо в них присутня анотація Imperio - відображає модифікатор доступу, тип, назву поля та значення анотації
    public void inspectFieldsAnnotation(){
        Class<Human> h = Human.class;
        Field[] fields = h.getDeclaredFields();
        System.out.println("List of declared fields with @Imperio annotation");
        for(Field f : fields){
            f.setAccessible(true);
            if(f.isAnnotationPresent(Imperio.class)) {
                System.out.print(Modifier.toString(f.getModifiers()) +" "+ f.getType().toString() +" "+ f.getName());
                System.out.print(", include annotation @Imperion(name = \"" +
                        f.getAnnotation(Imperio.class).name() +
                        "\" , age = \"" +
                        f.getAnnotation(Imperio.class).age());
                System.out.println("\"");
            }
        }
    }

    //Дає інформацію про методи класу даного об'єкту
    public void showMethodsInfo(Object o){
        Class clazz = o.getClass();
        Method[] methods = clazz.getDeclaredMethods();
        for (Method m : methods){
            System.out.println("Method name " + m.getName());
            System.out.println("Method return type " + m.getReturnType());
            if (m.getParameterCount() ==0) System.out.println("Method has no parameters");
            for(Class c : m.getParameterTypes()){
                System.out.println("Method parameter " + c.getCanonicalName());
            }
            System.out.println();
        }
    }

    public void myMethod(String a, int... args){
        System.out.print(a);
        for(int i : args) System.out.print(i + " ");
        System.out.println();
    }
    public String myMethod(String... args){
        StringBuilder tmp = new StringBuilder();
        for (String s : args){
            tmp.append(s + " ");
        }
        return tmp.toString();
    }

    private boolean isOld(){
        if (age>=30) return true;
        return false;
    }

    public static void main(String[] args) {
        Human h = new Human("John", "Smith", 30);
        h.inspectFieldsAnnotation();
        System.out.println();
        h.showMethodsInfo(h);
        try {
            //Methods invokes
            Method m = h.getClass().getDeclaredMethod("myMethod", String[].class);
            System.out.println(m.invoke(h,new Object [] {new String []{"One", "Misisipi", "two", "Misisipi"}}));

            m = h.getClass().getDeclaredMethod("myMethod", String.class, int[].class);
            m.invoke(h,new Object [] {"Numbers from 0 to 9 - ", new int[]{0,1,2,3,4,5,6,7,8,9}});
            System.out.println();

            m = h.getClass().getDeclaredMethod("isOld");
            m.setAccessible(true);
            System.out.println("This human is old? " + m.invoke(h));

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
